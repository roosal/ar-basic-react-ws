import users from '../data/user'
import pokemon from '../data/pokemon'
import pokemonData from '../data/pokemonData.json'

async function stall (stallTime = 1000) {
  await new Promise(resolve => setTimeout(resolve, stallTime))
}

const revivePokemon = (base, preset) => ({
  ...base,
  ...preset,
  stats: {
    base: base.stats,
    ivs: preset.iv,
    evs: preset.ev
  },
  types: base.type
})

export function getNumber (name) {
  console.log(name)
  console.log(pokemonData)
  return pokemonData.find(p => p.name === name).number
}

export async function login (mail, password) {
  await stall(500)

  let user = users.find(user => user.mail === mail)
  if (user) {
    if(user.password === password) {
      return {
        _id: user._id,
        token: { payload: {
          firstname: user.firstname,
          lastname: user.lastname
        }},
        team: user.team,
        pokemon: user.pokemon
      }
    }
  }

  return {
    error: 'Login failed.'
  }
}

export function pokemonList (start = 0, end = pokemonData.length) {
  // await stall(500)

  return pokemonData
    .slice(start, end)
    .map(p => ({ name: p.name, types: p.type, number: p.number }))
}

export function getPokemon ({
  id, name
}) {
  // await stall(500)

  // get full pokemon
  if (id !== undefined ) {
    let custom = pokemon.find(p => p._id = id)
    let base = pokemonData.find(p => p.name === custom.name)

    let x = revivePokemon(base, custom)
    console.log(x)
    return x
  }

  // get base pokemon
  if (name !== undefined) {
    return pokemonData.find(p => p.name === name)
  }

  
  return {
    error: `Can't find pokemon`
  }
}

export function isAuth () {
  if (localStorage.getItem('cea-token')) {
    return true
  } else {
    return false
  }
}

export function getUser () {
  return localStorage.getItem('cea-mail')
}