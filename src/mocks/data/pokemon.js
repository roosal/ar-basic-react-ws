export default [{
  _id: 'igfhospojsibvrber54b46e9g4erthst9rh4rthtbsbegreger',
  name: 'charmander',
  level: 3,
  experience: 500,
  iv: {
    hp: 6,
    attack: 21,
    defense: 9,
    spAttack: 12,
    spDefense: 16,
    speed: 3
  },
  ev: {
    hp: 0,
    attack: 2,
    defense: 0,
    spAttack: 5,
    spDefense: 2,
    speed: 0
  },
  nickname: 'charmander',
  abilities: [
    'growl',
    'ember'
  ],
  user: 'jggerqfzqhibqroubrzrz5gre8g9erq6grsgyrhgrgreg9re9ger'
}, {
  _id: 'hyebfspojsibvrber54b46e9jfyztdbt9rh4rthtldnehtyenf',
  name: 'weedle',
  level: 3,
  experience: 500,
  iv: {
    hp: 8,
    attack: 15,
    defense: 6,
    spAttack: 26,
    spDefense: 25,
    speed: 11
  },
  ev: {
    hp: 0,
    attack: 0,
    defense: 0,
    spAttack: 0,
    spDefense: 0,
    speed: 0
  },
  nickname: 'weedle',
  abilities: [
    'stringshot',
    'tackle'
  ],
  user: 'jggerqfzqhibqroubrzrz5gre8g9erq6grsgyrhgrgreg9re9ger'
}]