import React, { Component } from 'react'
import './App.css'

import Header from './header/Header'

import { getPokemon, getNumber } from '../mocks/service/mockService'

class App extends Component {
  render() {
    let { level, name, text, types, abilities, evolutions, stats, category, ...pokemon } = getPokemon({id: 'igfhospojsibvrber54b46e9g4erthst9rh4rthtbsbegreger'})

    return (
      <div>
        <Header />
        <div className="container pokemon-details">
          <div className="container flex-parent">
            <div className="pokemon-image">
              <img src={`https://assets.pokemon.com/assets/cms2/img/pokedex/full/${pokemon.number}.png`} />
            </div>
            <div className="summary">
              <span className="pokemon-level">{level}</span>
              <h2>{ name }</h2>
              <p>{ category }</p>
              <hr />
              <p>{ text }</p>
              <div style={{width: '100px'}}><h3>Types</h3></div>
              <div className="flex-parent" style={{marginLeft: '10px'}}>
                { types.map(type => <div key={type} className={`type ${type}`}>{ type }</div>)}
              </div>
              <div style={{width: '100px'}}><h3>Evolutions</h3></div>
              <div className="flex-parent evolutions" style={{marginLeft: '10px'}}>
                {
                  evolutions.map(evolution => (
                    <React.Fragment key={evolution.name}>
                      <img src={`https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${getNumber(evolution.name)}.png`} className={`evolution ${name === evolution.name ? 'active' : ''}`} />
                      <div className="arrow-right">&rArr;</div>
                    </React.Fragment>
                  ))
                }
              </div>
            </div>
          </div>
          <div className="container flex-grid">
            <div className="abilities">
              <h3>Abilities</h3>
              {
                abilities.map(ability => (
                  <div key={ability} className="flex-grid">
                    <div className="ability">{ability}</div>
                  </div>
                ))
              }
            </div>
            <div className="stats">
              <h3>Stats</h3>
              {
                Object.keys(stats.base).map(stat => {
                  const statValue = stats.base[stat] + stats.ivs[stat] + stats.evs[stat]
                  const barWidth = statValue / 200 * 100

                  return (
                    <div key={stat} className="flex-grid">
                      <div className="stat" style={{flexGrow: 0.5}}>{stat}</div>
                      <div>
                        <div className="stat-bar" style={{width: barWidth + '%', backgroundColor: barWidth >= 70 ? 'green' : barWidth >= 40 ? 'orange' : 'red'}}></div>
                      </div>
                    </div>
                  )
                })
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App
