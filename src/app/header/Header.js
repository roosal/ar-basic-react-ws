import React from 'react'

import { isAuth, getUser } from '../../mocks/service/mockService'

const Header = props => (
  <header className="top-menu">
    <div className="top-menu-user" style={{float: 'right'}}>
      {
        isAuth()
          ? (
            <React.Fragment>
              <img className="avatar" src="" />
              { getUser() }
            </React.Fragment>
          )
          : (
            <React.Fragment>
              You are not logged in
            </React.Fragment>
          )
      }
    </div>
    <h1 className="alt-text">
      <span style={{marginRight: '20px', color: 'white', fontWeight: 'bold', cursor: 'pointer'}}>&#x39e;</span>
      Collect all pokemons
    </h1>
  </header>
)

export default Header
